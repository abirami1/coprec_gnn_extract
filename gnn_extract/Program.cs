﻿using System;
using MySql.Data.MySqlClient;
using System.Net.Http;
using System.IO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace gnn_extract
{
    class Program
    {
        static string c_date = DateTime.Now.ToString("yyyy-MM-dd");
        private static readonly HttpClient client = new HttpClient();
        static void Main(string[] args)
        {try
            {
                string connstring_setupdb = "Server = localhost; Database = setupsheetdb; Uid = root; Pwd = ;SslMode=none";
                MySqlConnection mcon_setup = new MySqlConnection(connstring_setupdb);
                mcon_setup.Open();

                var files_coprec = Directory.GetFiles(@"F:\xampp\htdocs\alfadockpro\factory_layout\651\GNN", "*.csv");
                var files = Directory.GetFiles(@"F:\xampp\htdocs\alfadockpro\factory_layout\498\GNN", "*.csv");

               foreach (String f in files)
               {
                   string fname = System.IO.Path.GetFileNameWithoutExtension(f);


                  if (fname.StartsWith("RPA"))
                       extract_gnn_data(f, mcon_setup);
                   else
                       extract_gnn_data_arigis(f, mcon_setup);
                       
            }
                foreach (String f1 in files_coprec)
                {
                    extract_gnn_data_coprec(f1, mcon_setup);

                }
            }
            catch (Exception e)
            {
                using (StreamWriter w = File.AppendText("log.txt"))
                {
                    Log("" + e, w);
                }
            }
        }
        public static void Log(string logMessage, TextWriter w)
        {
            w.Write("\r\nLog Entry : ");
            w.WriteLine($"{DateTime.Now.ToLongTimeString()} {DateTime.Now.ToLongDateString()}");
            w.WriteLine("  :");
            w.WriteLine($"  :{logMessage}");
            w.WriteLine("-------------------------------");
        }
        public static void extract_gnn_data_arigis(string path, MySqlConnection mcon2)
        {
            try
            {
                string schdname = Path.GetFileNameWithoutExtension(path);
                using (StreamReader csv1 = new StreamReader(File.OpenRead(path)))
                {
                    int counter = 1;
                    while (!csv1.EndOfStream)
                    {
                        var line = csv1.ReadLine();
                        var csv = line.Split(',');
                        string compid = csv[0];
                        string mono = csv[1];
                        string pgmno = csv[2];
                        string partno = csv[3];
                        int tot_count = Convert.ToInt32(csv[4]);
                       /* using (StreamWriter w = File.AppendText("log.txt"))
                        {
                            Log("" + mono + pgmno + pgmno, w);
                        }
                        */
                        string CmdText = "INSERT IGNORE INTO setupsheetarigis1 VALUES(@sid,@machinename,@materialname,@materialsize,@filename,@date,@processed,@sheets,@processingtime,@starttime,@endtime,@programname,@pduedate,@qty,@partnum,@compid,@partname,@comment,@orderno,@schedulename)";
                        MySqlCommand cmd = new MySqlCommand(CmdText, mcon2);
                        cmd.Parameters.AddWithValue("@sid", pgmno + "p" + counter);
                        cmd.Parameters.AddWithValue("@machinename", "ACIES");
                        cmd.Parameters.AddWithValue("@materialname", "SPCC1.2");
                        cmd.Parameters.AddWithValue("@materialsize", "2438.00 X 1219.00");
                        cmd.Parameters.AddWithValue("@filename", "Test.pdf");
                        cmd.Parameters.AddWithValue("@sheets", 1);
                        cmd.Parameters.AddWithValue("@processingtime", "00:5:00");
                        cmd.Parameters.AddWithValue("@starttime", "0000-00-00 00:00:00");
                        cmd.Parameters.AddWithValue("@endtime", "0000-00-00 00:00:00");
                        cmd.Parameters.AddWithValue("@comment", "");
                        cmd.Parameters.AddWithValue("@programname", pgmno);
                        cmd.Parameters.AddWithValue("@partnum", partno);
                        cmd.Parameters.AddWithValue("@partname", "");
                        cmd.Parameters.AddWithValue("@qty", tot_count);
                        cmd.Parameters.AddWithValue("@orderno", mono);
                        cmd.Parameters.AddWithValue("@schedulename", schdname);
                        cmd.Parameters.AddWithValue("@compid", compid);
                        cmd.Parameters.AddWithValue("@processed", 0);
                        cmd.Parameters.AddWithValue("@pduedate", c_date);
                        cmd.Parameters.AddWithValue("@date", c_date);
                        cmd.ExecuteNonQuery();
                        counter++;
                    }

                }
            }
            catch (Exception e)
            {
                using (StreamWriter w = File.AppendText("log.txt"))
                {
                    Log("" + e, w);
                }
            }
        }
        public static void extract_gnn_data(string path, MySqlConnection mcon2)
        {
            try
            {
                using (StreamReader csv1 = new StreamReader(File.OpenRead(path)))
                {
                    int counter = 1;
                    while (!csv1.EndOfStream)
                    {
                        var line = csv1.ReadLine();
                        var csv = line.Split(',');
                        string compid = csv[0];
                        string mono = csv[1];
                        string pgmno = csv[2];
                        string partno = csv[3];
                        int tot_count = Convert.ToInt32(csv[4]);
                        /* using (StreamWriter w = File.AppendText("log.txt"))
                         {
                             Log("" + mono+ pgmno+ pgmno, w);
                         }*/

                        string CmdText = "INSERT IGNORE INTO partinfo_kamishiro VALUES(@idcsv,@partname,@pgmname,@total,@mono,@compid,@status)";
                        MySqlCommand cmd = new MySqlCommand(CmdText, mcon2);
                        cmd.Parameters.AddWithValue("@idcsv", pgmno + "p" + counter);
                        cmd.Parameters.AddWithValue("@partname", partno);
                        cmd.Parameters.AddWithValue("@pgmname", pgmno);
                        cmd.Parameters.AddWithValue("@total", tot_count);
                        cmd.Parameters.AddWithValue("@mono", mono);
                        cmd.Parameters.AddWithValue("@compid", compid);
                        cmd.Parameters.AddWithValue("@status", 0);
                        cmd.ExecuteNonQuery();
                        counter++;
                    }

                }
            }
            catch (Exception e)
            {
                using (StreamWriter w = File.AppendText("log.txt"))
                {
                    Log("" + e, w);
                }
            }
        }
        public static DateTime ConvertJSTToUTCwithTimeZone(DateTime localDateTime)
        {
            localDateTime = DateTime.SpecifyKind(localDateTime, DateTimeKind.Unspecified);
            return TimeZoneInfo.ConvertTimeToUtc(localDateTime,
              TimeZoneInfo.FindSystemTimeZoneById("Tokyo Standard Time"));
        }
        public static DateTime JapanDateTime(DateTime currentTime)
        {
            DateTime cstTime = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(currentTime, "Tokyo Standard Time", "Tokyo Standard Time");
            return cstTime;
        }
        public static async void extract_gnn_data_coprec(string path, MySqlConnection mcon2)
        {
            try
            {
                using (StreamReader csv1 = new StreamReader(File.OpenRead(path)))
                {
                    int counter = 1;

                    var headerLine = csv1.ReadLine();
                    var line = "";
                    while ((line = csv1.ReadLine()) != null)
                    {
                        var csv = line.Split(',');
                        string compid = csv[0];
                        string pgmno = csv[1];
                        string mono = csv[3];
                        string partno = csv[4];
                        int tot_count = 1;
                        try
                        {
                            tot_count = Convert.ToInt32(csv[5]);
                        }
                        catch (Exception)
                        {
                            tot_count = 1;
                        }
                        string neststrttime = csv[6];
                        string nestendtime = csv[7];
                        string operator_name = "";
                        try { operator_name = csv[8]; } catch (Exception) { }
                        string utc_time_start_val = "";
                        string utc_time_end_val = "";
                        if (!String.IsNullOrEmpty(neststrttime) && !String.IsNullOrEmpty(neststrttime))
                        {
                            string[] nstvals = neststrttime.Split('-');
                            string srttimeval = nstvals[0] + "-" + nstvals[1] + "-" + nstvals[2] + " " + nstvals[3] + ":" + nstvals[4] + ":" + nstvals[5];

                            string[] nstendvals = nestendtime.Split('-');
                            string srttimeendval = nstendvals[0] + "-" + nstendvals[1] + "-" + nstendvals[2] + " " + nstendvals[3] + ":" + nstendvals[4] + ":" + nstendvals[5];

                            DateTime thisstartTime = DateTime.ParseExact(srttimeval, "yyyy-MM-dd HH:mm:ss",
                                           System.Globalization.CultureInfo.InvariantCulture);
                            DateTime jptime = JapanDateTime(thisstartTime);
                            DateTime utctime = ConvertJSTToUTCwithTimeZone(jptime);
                            utc_time_start_val = utctime.ToString("yyyy-MM-dd HH:mm:ss");

                            DateTime thisendTime = DateTime.ParseExact(srttimeendval, "yyyy-MM-dd HH:mm:ss",
                                          System.Globalization.CultureInfo.InvariantCulture);
                            jptime = JapanDateTime(thisendTime);
                            utctime = ConvertJSTToUTCwithTimeZone(jptime);
                            utc_time_end_val = utctime.ToString("yyyy-MM-dd HH:mm:ss");
                        }
                        /*string CmdText = "INSERT IGNORE INTO partinfo_coprec VALUES(@idcsv,@partname,@pgmname,@total,@mono,@compid,@status)";
                        MySqlCommand cmd = new MySqlCommand(CmdText, mcon2);
                        cmd.Parameters.AddWithValue("@idcsv", pgmno + "p" + counter);
                        cmd.Parameters.AddWithValue("@partname", partno);
                        cmd.Parameters.AddWithValue("@pgmname", pgmno);
                        cmd.Parameters.AddWithValue("@total", tot_count);
                        cmd.Parameters.AddWithValue("@mono", mono);
                        cmd.Parameters.AddWithValue("@compid", compid);
                        cmd.Parameters.AddWithValue("@status", 0);
                        cmd.ExecuteNonQuery();*/



                        string query = string.Format(@"UPDATE `setupsheetdb`.setupsheetcoprecbend SET orderno='{0}',comqty={3},operator='{4}' where partnum='{1}' AND programname='{2}' AND orderno=''", mono, partno, pgmno, tot_count, operator_name);

                        using (MySqlCommand command = new MySqlCommand(query, mcon2))
                        {
                            int row = command.ExecuteNonQuery();
                            if (row == 1)
                            {
                                await update_GPN(partno, 1, "" + tot_count, "2705", mono, "ﾌﾟﾛｸﾞﾗﾑ/ﾈｽﾃｨﾝｸﾞ", utc_time_start_val, utc_time_end_val,operator_name);
                                await update_GPN1(partno, 1, "" + tot_count, "2705", mono, "ﾌﾟﾛｸﾞﾗﾑ/ﾈｽﾃｨﾝｸﾞ", utc_time_start_val, utc_time_end_val, operator_name);
                               
                            }
                        }

                        counter++;


                    }
                }
            }
            catch (Exception e)
            {
                using (StreamWriter w = File.AppendText("log.txt"))
                {
                    Log("" + e, w);
                }

            }
        }
        public static async Task update_GPN1(String partno, int status, string qt, string userid, string orderno, string processname,string utc_start_time_val, string utc_end_time_val,string operator_name)
        {
            var str_arg = "{\"key\":\"7aBNl26gf0yaxEDrFJpE\",\"method\":\"name\",\"order_number\":\"" + orderno + "\",\"product_number\":\"" + partno + "\",\"process_name\":\"" + processname + "\",\"name\":\"sp1\",\"start_date\":\""+ utc_start_time_val + "\",\"end_date\":\""+ utc_end_time_val + "\",\"status\":\"" + status + "\",\"qty\":\"" + qt + "\",\"operator_name\":\"" + operator_name + "\",\"vFactory\":\"1\"}";
            if (String.IsNullOrEmpty(operator_name))
            {
                str_arg = "{\"key\":\"7aBNl26gf0yaxEDrFJpE\",\"method\":\"name\",\"order_number\":\"" + orderno + "\",\"product_number\":\"" + partno + "\",\"process_name\":\"" + processname + "\",\"name\":\"sp1\",\"start_date\":\"" + utc_start_time_val + "\",\"end_date\":\"" + utc_end_time_val + "\",\"status\":\"" + status + "\",\"qty\":\"" + qt + "\",\"vFactory\":\"1\"}";
            }
            using (StreamWriter w = File.AppendText("log.txt"))
            {
                Log(str_arg, w);
            }
            var values = new Dictionary<string, string>
            {

                { "args", str_arg }
            };

            var content = new FormUrlEncodedContent(values);

            var response = await client.PostAsync("https://coprec-dev.alfa-erp.com/api/set_alfaerp_data/SISUBMITPROCESS", content);

            await response.Content.ReadAsStringAsync();

        }
        public static async Task update_GPN(String partno,  int status, string qt, string userid,string orderno,  string processname, string utc_start_time_val, string utc_end_time_val,string operator_name)
        {
            var str_arg = "{\"key\":\"7aBNl26gf0yaxEDrFJpE\",\"method\":\"name\",\"order_number\":\""+ orderno + "\",\"product_number\":\""+ partno + "\",\"process_name\":\""+ processname + "\",\"name\":\"sp1\",\"start_date\":\""+ utc_start_time_val + "\",\"end_date\":\"" + utc_end_time_val + "\",\"status\":\"" + status + "\",\"qty\":\"" + qt + "\",\"operator_name\":\"" + operator_name + "\",\"vFactory\":\"1\"}";
            if (String.IsNullOrEmpty(operator_name))
            {
                str_arg = "{\"key\":\"7aBNl26gf0yaxEDrFJpE\",\"method\":\"name\",\"order_number\":\"" + orderno + "\",\"product_number\":\"" + partno + "\",\"process_name\":\"" + processname + "\",\"name\":\"sp1\",\"start_date\":\"" + utc_start_time_val + "\",\"end_date\":\"" + utc_end_time_val + "\",\"status\":\"" + status + "\",\"qty\":\"" + qt + "\",\"vFactory\":\"1\"}";
            }
            using (StreamWriter w = File.AppendText("log.txt"))
            {
                Log(str_arg, w);
            }
            var values = new Dictionary<string, string>
            {
    
                { "args", str_arg }
            };

            var content = new FormUrlEncodedContent(values);

            var response = await client.PostAsync("https://coprecnnf.alfa-erp.com/api/set_alfaerp_data/SISUBMITPROCESS", content);

            await response.Content.ReadAsStringAsync();

        }
    }
    }
